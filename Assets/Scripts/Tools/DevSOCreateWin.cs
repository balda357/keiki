﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DevSOCreateWin : EditorWindow 
{
    string nameSO = "";
    string type = "";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;

    [MenuItem("Dev Tools/Create Scriptable Object")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(DevSOCreateWin));
    }

    void OnGUI()
    {
        GUILayout.Label("Scriptable Object creator", EditorStyles.boldLabel);
        nameSO = EditorGUILayout.TextField("SO name: ", nameSO);
        type = EditorGUILayout.TextField("SO type: ", type);

        if (GUI.Button(new Rect(10, 70, 50, 30), "Create"))
            CreateMyAsset();
    }

    public void CreateMyAsset()
    {
        var scriptableObject = ScriptableObject.CreateInstance(type);

        AssetDatabase.CreateAsset(scriptableObject, "Assets/" + nameSO + ".asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = scriptableObject;
    }
}
#endif

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(AnimalView))]
public class Animal : MonoBehaviour
{

    [HideInInspector] public AnimalView animalView;

    private void Awake()
    {
        animalView = GetComponent<AnimalView>();
    }


    public void OnCorrectAnswer()
    {
        animalView.CorrectAnswer();
    }


    public void OnIncorrectAnswer()
    {
        animalView.IncorrectAnswer();
    }



}

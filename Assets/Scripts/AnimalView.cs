﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

[RequireComponent(typeof(Animal))]
public class AnimalView : MonoBehaviour
{
    private SkeletonGraphic SkeletonGraphic;

    [SpineSlot] public string slot;
    public Vector3 tilePosition;

    private Spine.AnimationState animationState;


    [SpineAnimation] public string talkAnimation;
    [SpineAnimation] public string correctAnimation;
    [SpineAnimation] public string incorrectAnimation;

    public AudioClip answerAudio;

    private void Awake()
    {
        SkeletonGraphic = GetComponent<SkeletonGraphic>();
        animationState = SkeletonGraphic.AnimationState;
    }

    private void Start()
    {
        Spine.Bone bone = GetComponent<SkeletonGraphic>().Skeleton.FindSlot(slot).Bone; //.GetWorldPosition(transform);

        tilePosition = transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));//tmp;// new Vector3(tmp.x, 0, tmp.y);

        StartAnswer();
    }

    public void StartAnswer()
    {
        animationState.SetAnimation(0, talkAnimation, false);
        animationState.SetAnimation(1, "Idle", true);

        AudioManager.Instance.Play(answerAudio);
    }

    public void Tap()
    {
        animationState.SetAnimation(0, "Tap", false);
        animationState.SetAnimation(1, "Idle", true);

    }

    public void CorrectAnswer()
    {
        animationState.SetAnimation(0, correctAnimation, false);
        animationState.SetAnimation(1, "Idle", true);
        AudioManager.Instance.PlayCorrect();
    }

    public void IncorrectAnswer()
    {
        animationState.SetAnimation(0, "Tap", false);
        animationState.SetAnimation(1, "Idle", true);
        AudioManager.Instance.PlayIncorrect();
    }

}

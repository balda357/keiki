﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private AudioSource audioSource;


    public List<AudioClip> CorrectAnswers;

    public List<AudioClip> IncorrectAnswers;



    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayCorrect()
    {
        Play(CorrectAnswers[Random.Range(0, CorrectAnswers.Count)]);
    }

    public void PlayIncorrect()
    {
        Play(IncorrectAnswers[Random.Range(0, IncorrectAnswers.Count)]);
    }

    public void Play(AudioClip audioClip)
    {
        audioSource.Stop();
        audioSource.clip = audioClip;
        audioSource.Play();
    }


    #region Singleton

    private static AudioManager _instance;

    public static AudioManager Instance
    {
        get
        {
            if (_instance == null) _instance = FindObjectOfType<AudioManager>();
            return _instance;
        }
    }

    #endregion





}

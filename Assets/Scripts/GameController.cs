﻿using DG.Tweening;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private Transform hintFinger;


    [SerializeField] private List<Animal> animalsList;
    [SerializeField] private Transform root;


    private Animal currentAnimal;
    private int indexOfCurrent;

    private void Start()
    {
        StartCoroutine(NextAnimal(0));
    }


    private IEnumerator NextAnimal(int delay)
    {
        yield return new WaitForSeconds(delay);


        int newIndex;
        while ((newIndex = Random.Range(0, 6)) == indexOfCurrent) { }

        indexOfCurrent = newIndex;

        currentAnimal = Instantiate(animalsList[indexOfCurrent], root);
    }


    public void TapOnTail(Transform t)
    {
       // t.DOMove(currentAnimal.animalView.tilePosition, 1, false);
       // t.DORestart();

        if (t.gameObject.name == currentAnimal.name.Replace("(Clone)", ""))
        {

            currentAnimal.OnCorrectAnswer();
            Destroy(currentAnimal.gameObject, 1.5f);
            StartCoroutine(NextAnimal(2));
        }
        else
        {
            currentAnimal.OnIncorrectAnswer();
        }
        HideHint();
    }


    private float timer;
    private bool isHintInvoked = false;

    private void Update()
    {
        if (!isHintInvoked)
        {
            timer += Time.deltaTime;

            if (timer >= 7)
            {
                isHintInvoked = true;
                InvokeHint();
            }
        }
    }


    private void InvokeHint()
    {
        currentAnimal.animalView.StartAnswer();

        hintFinger.position = GameObject.Find(currentAnimal.name.Replace("(Clone)", "")).transform.position;
        hintFinger.gameObject.SetActive(true);
        hintFinger.DOScale(1.2f, .5f).SetLoops(-1, LoopType.Yoyo);
        hintFinger.DORestart();
    }

    public void HideHint()
    {
        timer = 0;
        isHintInvoked = false;
        hintFinger.gameObject.SetActive(false);
    }

}
